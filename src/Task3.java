import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);


        ///Task 3 // While cycle
        //Факториал числа.

        System.out.println("Введите число ");
        int n = in.nextByte();
        int p = 1;
        int i = 1;
        while (i < n) {

            i += 1;
            p *= i;
        }
        System.out.println("Факториал числа: " + n + " = " + p);


        // Task 3// Recursive
        System.out.println("Введите число");
        int num = in.nextByte();
        System.out.println("Факториал числа: " + num + " = " + fac(num));

    }

    // Recursive function
    public static int fac(int n) {

        if (n == 1)
            return 1;

        return n * fac(n - 1);
    }
}